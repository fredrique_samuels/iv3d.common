/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.iv3d.common.utils.ExceptionUtils;


/**
 * <p>
 * A utility class for loading resource form package folders.
 * </p>
 */
public class ResourceLoader {
	
	private static Logger logger = Logger.getLogger(ResourceLoader.class);
	
	/**
	 * Read a resource as an input stream.
	 * 
	 * @param filename The resource filename.
	 * @return The resource stream or <code>null</code> if the resource could not be loaded.
	 */
	public static InputStream read(String filename) {
		InputStream stream = ResourceLoader.class.getResourceAsStream(filename);
		if(stream==null) {
			stream = loadFile(filename);
		}
		return stream;
	}
	
	public static InputStream read(URL url) {
		return read(url.getPath());
	}
	
	public static String readAsString(String f) {
		InputStream in = read(f);
		return readInputStreamAsString(in);
	}

	private static String readInputStreamAsString(InputStream in) {
		StringBuilder source = new StringBuilder();
		BufferedReader reader = null;
		try {
			String line;
			long lineCount=0;
			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			while ((line = reader.readLine()) != null){
				lineCount++;
				if(lineCount>1) {
					source.append('\n');
				}
				source.append(line);
			}
			return source.toString();
		} catch (IOException e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
		} finally {
			try {
				if(reader!=null)
					reader.close();
			} catch (IOException exc) {
				logger.warn(ExceptionUtils.getStackTrace(exc));
			}
		}
		
		return null;
	}

	private static FileInputStream loadFile(String filename) {
		File file = new File(filename);
		if(file.exists()) {
			try {
				return new FileInputStream(file);
			} catch (FileNotFoundException e) {
				logger.warn(ExceptionUtils.getStackTrace(e));
			}
		}
		return null;
	}
	
	/**
	 * Get a resource {@link URL} from it's filename.
	 * 
	 * @param filename The resource filename.
	 * @return The resource {@link URL}.
	 */
	public static URL getUrl(String filename) {
		URL resource = ResourceLoader.class.getResource(filename);
		if(resource==null) {
			try {
				return new File(filename).toURI().toURL();
			} catch (MalformedURLException e) {
				return null;
			}
		}
		return resource;
	}
	
	public static URL getParentUrlOrNull(String filename) {
		try {
			URL url = ResourceLoader.getUrl(filename);
			String path = url.getFile();
			File parentFile = new File(path).getParentFile();
			if(parentFile==null){return null;}
			return parentFile.toURI().toURL();
		} catch (MalformedURLException e) {
			return null;
		}
	}
	
	private ResourceLoader(){}

	public static String readAsString(File file) {
		try {
			return readInputStreamAsString(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
