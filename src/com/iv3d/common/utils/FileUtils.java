/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class FileUtils {
	
	public static final class FileOperationError extends RuntimeException {
		public FileOperationError(IOException e) {
			super(e);
		}

		private static final long serialVersionUID = 1L;
	}

	public static String contentsAsString(File file) {
		try {
			return readInputStreamAsString(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new FileOperationError(e);
		}
	}
	
	private static String readInputStreamAsString(InputStream in) {
		StringBuilder source = new StringBuilder();
		BufferedReader reader = null;
		try {
			String line;
			long lineCount=0;
			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			while ((line = reader.readLine()) != null){
				lineCount++;
				if(lineCount>1) {
					source.append('\n');
				}
				source.append(line);
			}
			return source.toString();
		} catch (IOException e) {
			throw new FileOperationError(e);
		} finally {
			try {
				if(reader!=null)
					reader.close();
			} catch (IOException exc) {
				// ignore close
			}
		}
	}

	public static void writeStringToFile(File file, String content) {
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				throw new FileOperationError(e);
			}
		}
		
		FileOutputStream fop = null;
		try {

			fop = new FileOutputStream(file);

			byte[] contentInBytes = content.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

		} catch (IOException e) {
			throw new FileOperationError(e);
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				//ignore
			}
		}
		
	}
}
