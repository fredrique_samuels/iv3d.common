/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common;

/**
 * A representation of color made up of 4 components.
 * Component values range from 0.0-1.0 . 
 */
public final class NormalizedColor {
	final float r;
	final float g;
	final float b;
	final float a;

	/**
	 * <p>
	 * Default constructor.
	 * </p>
	 */
	public NormalizedColor() {
		this(1, 1, 1, 1);
	}

	/**
	 * <p>
	 * Value constructor.
	 * </p>
	 * 
	 * @param r The <i>red</i> component.
	 * @param g The <i>green</i> component.
	 * @param b The <i>blue</i> component.
	 * @param a The <i>alpha</i> component.
	 */
	public NormalizedColor(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/**
	 * <p>
	 * Value constructor.
	 * </p>
	 * 
	 * @param r The <i>red</i> component.
	 * @param g The <i>green</i> component.
	 * @param b The <i>blue</i> component.
	 */
	public NormalizedColor(float r, float g, float b) {
		this(r, g, b, 1);
	}

	/**
	 * <p>
	 * Copy constructor.
	 * </p>
	 * 
	 * @param color The color to copy.
	 */
	public NormalizedColor(NormalizedColor color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = color.a;
	}

	/**
	 * @return The <code>red</code> component.
	 */
	public float getRed() {
		return r;
	}

	/**
	 * @return The <code>green</code> component.
	 */
	public float getGreen() {
		return g;
	}

	/**
	 * @return The <code>blue</code> component.
	 */
	public float getBlue() {
		return b;
	}

	/**
	 * @return The <code>alpha</code> component.
	 */
	public float getAlpha() {
		return a;
	}
	
	@Override
	public String toString() {
		return String.format("<%s r=%.3f g=%.3f b=%.3f a=%.3f>", NormalizedColor.class.getName(), r, g, b, a);
	}
	
	/**
	 * @return A array of {r, g, b, a}.
	 */
	public final float[] toArray() {
		return new float[] {r, g, b, a};
	}
	
	/**
	 * @return A {@link Color} instance for this Normalized color.
	 */
	public final Color toColor() {
		return new Color(this);
	}

}
