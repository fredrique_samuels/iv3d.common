package com.iv3d.common.pagination;

/**
 * Public interface for all data generating objects.
 */
public interface DataGenerator<T> {	
	
	/**
	 * Get the total number of result in the data source.
	 * This is used along with the pageSize to determine the page count.
	 * 
	 * @return The total number of results in the data source.
	 */
	long totalRecords();
	
	/**
	 * Generate the data from the given index with maximum number of records
	 * not exceeding <tt>max</tt>.
	 * 
	 * @param offset The record offset.
	 * @param max The maximum number of records to return.
	 * @return The records results.
	 */
	T[] generate(long offset, long maxRecords);
}
