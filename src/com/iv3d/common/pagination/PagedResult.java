package com.iv3d.common.pagination;

/**
 * Public interface for paginated result values.
 *
 * @param <T> The page type.
 */
public interface PagedResult<T> {
	
	/**
	 * @return The page contents.
	 */
	T[] data();
	
	/**
	 * @return The total number of pages
	 */
	long totalPages();
	
	/**
	 * @return the current page number.
	 */
	long currentPage();
	
	/**
	 * @return Number of result items on this page.
	 */
	long size();
	
	/**
	 * @return  <code>true</code> if this is not the last page.
	 */
	boolean hasNext();
	
	/**
	 * @return <code>true</code> if this is not the first page.
	 */
	boolean hasPrev();
	
	/**
	 * @return The next page or the current one if it is the last page.
	 */
	PagedResult<T> next();
	
	/**
	 * @return The previous page or the current page if this is the first page.
	 */
	PagedResult<T> prev();
}
