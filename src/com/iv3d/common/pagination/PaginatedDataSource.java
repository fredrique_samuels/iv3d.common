package com.iv3d.common.pagination;

/**
 * Public abstract data source for large data queries.
 */
public class PaginatedDataSource<T> {
	
	private final long maxPageSize;
	private final long pageCount;
	private final DataGenerator<T> dataGenerator;

	/**
	 * @param maxPageSize The maximum number of records per page.
	 */
	public PaginatedDataSource(int maxPageSize, DataGenerator<T> dataGenerator) {
		this.maxPageSize = maxPageSize;
		this.dataGenerator = dataGenerator;
		this.pageCount = calculatePageCount(maxPageSize);
	}

	private long calculatePageCount(int pageSize) {
		long totalRecords = dataGenerator.totalRecords();
		long res = totalRecords%pageSize;
		long baseCount = (totalRecords-res)/pageSize;
		long buffer = res==0?0:1;
		return baseCount+buffer;
	}

	/**
	 * Get the page by the the page number. 
	 * <p>
	 * If the page<=1, the page 1 is returned if any.
	 * If the page>maximum the the last page is returned.
	 * </p>
	 * @param page The page number to get.
	 * @return The page results.
	 */
	public PagedResult<T> get(long page) {
		return new PagedResultImpl(page);
	}
	
	class PagedResultImpl implements PagedResult<T> {

		private T[] data;
		private long pageNumber;
		private boolean hasNext;
		private boolean hasPrev;

		public PagedResultImpl(long page) {
			this.pageNumber = clipPageNumber(page);
			this.data = generateData();
			this.hasNext = pageNumber<pageCount;
			this.hasPrev = pageNumber>1;
		}

		private T[] generateData() {
			long multiplier = pageNumber==0?0:pageNumber-1;
			long offset = multiplier*maxPageSize;
			return dataGenerator.generate(offset, maxPageSize);
		}

		private long clipPageNumber(long page) {
			if(pageCount==0) {
				return 0;
			}
			
			if(page<=0 && pageCount>0) {
				return 1;
			}
			
			if(page>pageCount) {
				return pageCount;
			}
			return page;
		}

		@Override
		public T[] data() {
			return data;
		}

		@Override
		public long totalPages() {
			return pageCount;
		}

		@Override
		public long currentPage() {
			return pageNumber;
		}

		@Override
		public long size() {
			return data.length;
		}

		@Override
		public boolean hasNext() {
			return hasNext;
		}

		@Override
		public boolean hasPrev() {
			return hasPrev;
		}

		@Override
		public PagedResult<T> next() {
			return new PagedResultImpl(pageNumber+1);
		}

		@Override
		public PagedResult<T> prev() {
			return new PagedResultImpl(pageNumber-1);
		}
		
	}
}
