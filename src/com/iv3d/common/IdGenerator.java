package com.iv3d.common;

public interface IdGenerator {
	String createId();
}
