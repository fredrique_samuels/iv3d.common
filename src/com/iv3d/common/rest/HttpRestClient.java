/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public final class HttpRestClient implements RestClient {

	private final RestTemplate restTemplate;

	HttpRestClient(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public HttpRestClient() {
		this(new RestTemplate());
	}

	@Override
	public <R> ResponseEntity<R> execute(Request request, Class<R> responseType) {
		if(request.getMethod()==HttpMethod.GET) {
			URI url = request.getUrl();
			return restTemplate.exchange(url, 
					HttpMethod.GET, 
					new HttpEntity<String>(request.getHeaders()), 
					responseType);
		} else if(request.getMethod()==HttpMethod.POST) {
			URI url = request.getUrl();
			return restTemplate.exchange(url, 
					HttpMethod.POST, 
					new HttpEntity<String>(request.getPayload(), request.getHeaders()), 
					responseType);
		}
		return null;
	}
	
	protected List<HttpMessageConverter<?>> buildMessageConverters() {
        HttpMessageConverter<?> formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter<?> stringHttpMessageConverternew = new StringHttpMessageConverter();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(formHttpMessageConverter);
        messageConverters.add(stringHttpMessageConverternew);
        return messageConverters;
    }

}
