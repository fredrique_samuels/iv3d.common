package com.iv3d.common;

public interface Filter<T> {
	boolean accept(T readFromFile);
}
