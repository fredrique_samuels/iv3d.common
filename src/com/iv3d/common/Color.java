/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.common;

/**
 * A representation of color made up of 4 components.
 */
public final class Color {

	
	public static final Color white = new Color(0xffffff);
	public static final Color WHITE = white;
	public static final Color lightGray = new Color(0xc0c0c0);
	public static final Color LIGHT_GRAY = lightGray;
	public static final Color gray = new Color(0x808080);
	public static final Color GRAY = gray;
	public static final Color darkGray = new Color(0x404040);
	public static final Color DARK_GRAY = darkGray;
	public static final Color black = new Color(0x000000);
	public static final Color BLACK = black;
	public static final Color red = new Color(0xff0000);
	public static final Color RED = red;
	public static final Color pink = new Color(0xffafaf);
	public static final Color PINK = pink;
	public static final Color orange = new Color(0xffc800);
	public static final Color ORANGE = orange;
	public static final Color yellow = new Color(0xffff00);
	public static final Color YELLOW = yellow;
	public static final Color green = new Color(0x00ff00);
	public static final Color GREEN = green;
	public static final Color magenta = new Color(0xff00ff);
	public static final Color MAGENTA = magenta;
	public static final Color cyan = new Color(0x00ffff);
	public static final Color CYAN = cyan;
	public static final Color blue = new Color(0x0000ff);
	public static final Color BLUE = blue;

	final int r;
	final int g;
	final int b;
	final int a;

	/** Internal mask for red. */
	private static final int RED_MASK = 255 << 16;
	private static final int GREEN_MASK = 255 << 8;
	private static final int BLUE_MASK = 255;
//	private static final int ALPHA_MASK = 255 << 24;

	/**
	 * <p>
	 * Default constructor.
	 * </p>
	 */
	public Color() {
		this(255, 255, 255, 255);
	}

	/**
	 * <p>
	 * Value constructor.
	 * </p>
	 * 
	 * @param r
	 *            The <i>red</i> component.
	 * @param g
	 *            The <i>green</i> component.
	 * @param b
	 *            The <i>blue</i> component.
	 * @param a
	 *            The <i>alpha</i> component.
	 */
	public Color(int r, int g, int b, int a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public Color(int value) {
		this((value & RED_MASK) >> 16, (value & GREEN_MASK) >> 8, value
				& BLUE_MASK, 255);
	}

	/**
	 * <p>
	 * Value constructor that sets red, green and blue to the same value.
	 * </p>
	 * 
	 * @param rgb
	 *            The <i>rgb</i> components.
	 * @param a
	 *            The <i>alpha</i> component.
	 */
	public Color(int rgb, int a) {
		this(rgb, rgb, rgb, a);
	}

	/**
	 * <p>
	 * Value constructor.
	 * </p>
	 * 
	 * @param r
	 *            The <i>red</i> component.
	 * @param g
	 *            The <i>green</i> component.
	 * @param b
	 *            The <i>blue</i> component.
	 */
	public Color(int r, int g, int b) {
		this(r, g, b, 255);
	}

	/**
	 * <p>
	 * Copy constructor.
	 * </p>
	 * 
	 * @param color
	 *            The color to copy.
	 */
	public Color(Color color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = color.a;
	}

	/**
	 * <p>
	 * Copy constructor.
	 * </p>
	 * 
	 * @param color
	 *            The color to copy.
	 */
	public Color(NormalizedColor color) {
		this.r = (int) (color.r * 255);
		this.g = (int) (color.g * 255);
		this.b = (int) (color.b * 255);
		this.a = (int) (color.a * 255);
	}

	/**
	 * @return The <code>red</code> component.
	 */
	public int getRed() {
		return r;
	}

	/**
	 * @return The <code>green</code> component.
	 */
	public int getGreen() {
		return g;
	}

	/**
	 * @return The <code>blue</code> component.
	 */
	public int getBlue() {
		return b;
	}

	/**
	 * @return The <code>alpha</code> component.
	 */
	public int getAlpha() {
		return a;
	}

	/**
	 * Normalize the color values to 1.0.
	 * 
	 * @return A 4 component array of color values in range 0.0-1.0.
	 */
	public NormalizedColor normalize() {
		return new NormalizedColor((float) (((float) r) / 255.0),
				(float) (((float) g) / 255.0), (float) (((float) b) / 255.0),
				(float) (((float) a) / 255.0));
	}

	@Override
	public boolean equals(Object arg0) {
		Color c = (Color) arg0;
		return r == c.r && g == c.g && b == c.b && a == c.a;
	}

	@Override
	public String toString() {
		return String.format("<%s r=%d g=%d b=%d a=%d>", Color.class.getName(),
				r, g, b, a);
	}

	public static Color decode(String str) {
		int intValue = Integer.decode(str).intValue();
		return new Color(intValue);
	}
	
	public final String toHexRgb() {
		return String.format("#%02x%02x%02x", r, g, b);
	}

	public final String toHexRgba() {
		return String.format("#%02x%02x%02x%02x", r, g, b, a);
	}

}
