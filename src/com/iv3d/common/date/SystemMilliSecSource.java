package com.iv3d.common.date;

public class SystemMilliSecSource implements ClockMilliSecSource {

	@Override
	public long get() {
		return System.currentTimeMillis();
	}

}
