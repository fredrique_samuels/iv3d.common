package com.iv3d.common.date;

public interface UtcDate {
	int getYear();
	int getMonth();
	int getDay();
	int getHour();
	int getMinute();
	int getSecond();
	String toIso8601String();
}
