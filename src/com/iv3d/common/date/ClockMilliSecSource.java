package com.iv3d.common.date;

public interface ClockMilliSecSource {
	long get();
}
