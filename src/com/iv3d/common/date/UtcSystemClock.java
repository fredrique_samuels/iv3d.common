package com.iv3d.common.date;

public class UtcSystemClock implements UtcClock {

	private ClockMilliSecSource millSource;

	public UtcSystemClock(ClockMilliSecSource millSource) {
		this.millSource = millSource;
	}

	@Override
	public UtcDate now() {
		return new UtcSystemDate(millSource.get());
	}

}
