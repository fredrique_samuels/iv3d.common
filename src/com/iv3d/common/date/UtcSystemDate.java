package com.iv3d.common.date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class UtcSystemDate implements UtcDate {

	private DateTime time;

	public UtcSystemDate(long currentTimeMillis) {
		time = new DateTime(currentTimeMillis).withZone(DateTimeZone.UTC);
	}

	@Override
	public String toIso8601String() {
		return time.toDateTimeISO().toString("yyyy-MM-dd'T'HH:mm:ssZ");
	}

	@Override
	public int getYear() {
		return time.getYear();
	}

	@Override
	public int getMonth() {
		return time.getMonthOfYear();
	}

	@Override
	public int getDay() {
		return time.getDayOfMonth();
	}

	@Override
	public int getHour() {
		return time.getHourOfDay();
	}

	@Override
	public int getMinute() {
		return time.getMinuteOfHour();
	}

	@Override
	public int getSecond() {
		return time.getSecondOfMinute();
	}
	

}
