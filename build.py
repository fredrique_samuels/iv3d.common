import os, sys

TEMPLATE_ANT = """
<project>

    <target name="release">
        <antcall target="clean"/>
        <antcall target="compile"/>
        <antcall target="jar"/>
        <antcall target="src_jar"/>
        <antcall target="clean"/>
    </target>
    
    <target name="clean">
        <delete dir="build"/>
    </target>

    <target name="compile">
        <mkdir dir="build/classes"/>
        <javac srcdir="src" destdir="build/classes">
            <classpath>
                %(class_path)s
            </classpath>
        </javac>
    </target>

    <target name="jar">
        <jar destfile="release/%(artifact)s.jar" basedir="build/classes"/>
    </target>

    <target name="src_jar">
        <jar destfile="release/%(artifact)s.sources.jar" basedir="src"/>
    </target>

</project>
"""

INSTALL_TEMPLATE = """mkdir %(artifact_path)s
copy release\\%(artifact)s.jar %(artifact_path)s\\%(artifact)s.jar
"""

MAVEN_REPO = os.environ['MAVEN_REPO']
artifact_folder_config = "iv3d"
libs = ["log4j/jars/log4j-1.2.9.jar"]
version = open('VERSION').read()
artifact = "%s.%s"%(open('ARTIFACT').read(), version)

artifact_folder = os.path.join(os.path.join(MAVEN_REPO, artifact_folder_config), "jars").replace("/", "\\\\")

libs_src = ""    
for l in libs:
    d =  os.path.join(MAVEN_REPO, l).replace("\\", "/")
    libs_src+="<pathelement path=\"%s\"/>"%d
    
contents = TEMPLATE_ANT%{'class_path':libs_src,
    'artifact':artifact}

open("build.xml",'w').write(contents)
open("install.bat",'w').write(
    INSTALL_TEMPLATE%{'artifact_path':artifact_folder,'artifact':artifact}
    )
