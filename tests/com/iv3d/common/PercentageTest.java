/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */


package com.iv3d.common;

import junit.framework.TestCase;

public class PercentageTest extends TestCase {
	
	public void testDefaultConstructor() {
		Percentage instance = new Percentage();
		assertEquals(0, instance.get());
	}
	
	public void testLessThan0Constructor() {
		Percentage instance = new Percentage(-1);
		assertEquals(0, instance.get());
	}
	
	public void testMoreThan100Constructor() {
		Percentage instance = new Percentage(101);
		assertEquals(100, instance.get());
	}
	
	public void testValueConstructor() {
		{
			Percentage instance = new Percentage(0);
			assertEquals(0, instance.get());
		}
		
		{
			Percentage instance = new Percentage(50);
			assertEquals(50, instance.get());
		}
		
		{
			Percentage instance = new Percentage(99);
			assertEquals(99, instance.get());
		}
	}
}
