/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */


package com.iv3d.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import junit.framework.TestCase;

public class ResourceLoaderTest extends TestCase {
	private String packageFile = "/com/iv3d/common/test.txt";
	private String folderFile = "tests/com/iv3d/common/test.txt";
	
	public void testPackageScope() throws IOException { 
		InputStream stream = ResourceLoader.read(packageFile);
		assertNotNull(stream);
		stream.close();
	}
	
	public void testRelativePath2() throws IOException { 
		InputStream stream = ResourceLoader.read(folderFile);
		assertNotNull(stream);
		stream.close();
	}
	
	public void testRelativePathUrl() throws IOException { 
		URL url = ResourceLoader.getUrl(folderFile);
		InputStream stream = ResourceLoader.read(url.getPath());
		assertNotNull(stream);
		stream.close();
	}
	
	public void testRelativePath() throws IOException { 
		InputStream stream = ResourceLoader.read(folderFile);
		assertNotNull(stream);
		stream.close();
	}
	
	public void testReadAsString() throws IOException { 
		String stream = ResourceLoader.readAsString(folderFile);
		assertNotNull(stream);
		assertEquals("line1\nline2", stream);
	}
	
	public void testReadAsStringForFile() {
		String stream = ResourceLoader.readAsString(new File(folderFile));
		assertNotNull(stream);
		assertEquals("line1\nline2", stream);
	}
}
