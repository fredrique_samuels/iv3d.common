/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.utils;

import java.io.File;

import junit.framework.TestCase;

public final class FileUtilsTest extends TestCase {
	private String folderFile = "tests/com/iv3d/common/test.txt";
	private File TEST_FILE_FOR_WRITING = new File("tests/com/iv3d/common/test_write.txt");
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		TEST_FILE_FOR_WRITING.delete();
	}
	
	public void testFileGetContetntsAsString() {
		String actual = FileUtils.contentsAsString(new File(folderFile));
		assertEquals("line1\nline2", actual);
	}
	
	public void testErrorIfFileDOesNotEaxisstOnRead() {
		try {
			FileUtils.contentsAsString(new File("NonExsistntFile"));
			fail();
		} catch (FileUtils.FileOperationError e) {
			
		}
	}
	
	public void testWriteStringToFile() {
		assertFalse(TEST_FILE_FOR_WRITING.exists());
		FileUtils.writeStringToFile(TEST_FILE_FOR_WRITING, "file content");
		assertTrue(TEST_FILE_FOR_WRITING.exists());

		String content = FileUtils.contentsAsString(TEST_FILE_FOR_WRITING);
		assertEquals("file content", content);
	}
}
