/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.utils;

import java.io.File;

import junit.framework.TestCase;

public class JsonUtilsTest extends TestCase {
	private final File TEST_FILE = new File("testfile.json");
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		TEST_FILE.delete();
	}
	
	public void testWriteObjectToFile() {
		assertFalse(TEST_FILE.exists());
		JsonUtils.writeToFile(TEST_FILE, new DemoObject().setX(5));
		
		assertTrue(TEST_FILE.exists());
		String actual = FileUtils.contentsAsString(TEST_FILE);
		assertEquals("{\"x\":5}", actual);
	}
	
	public void testReadObjectFromFile() {
		JsonUtils.writeToFile(TEST_FILE, new DemoObject().setX(55));
		DemoObject object = JsonUtils.readFromFile(TEST_FILE, DemoObject.class);
		assertEquals(55, object.getX());
	}
	
	public void testWriteObjectToString() {
		String actual = JsonUtils.writeToString(new DemoObject().setX(5));
		assertEquals("{\"x\":5}", actual);
	}
	
	public void testReadObjectFromString() {
		DemoObject actual = JsonUtils.readFromString("{\"x\":55}", DemoObject.class);
		assertEquals(55, actual.getX());
	}
}
