/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.rest;

import org.springframework.http.ResponseEntity;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;
import mockit.internal.expectations.TestOnlyPhase;

public class RestOperationTest extends TestCase {
	
	@Mocked
	HttpResponseParser<String, Integer> parser;
	@Mocked
	HttpResponseParser<Integer, String> parserIS;
	@Mocked 
	Request request;
	@Mocked
	RestClient client;
	@Mocked
	ResponseEntity<Integer> responseEntity;
	
	public void testConstruction() {
		RestOperation restOperation = new RestOperation<String, Integer>(request, parser, Integer.class);
	}
	
	public void testExchange() {
		new NonStrictExpectations() {
			{
				client.execute(request, Integer.class);
				result = responseEntity;
				
				parser.parse(responseEntity);
				result = "55";
			}
		};
		
		RestOperation<String, Integer> restOperation = new RestOperation<String, Integer>(request, parser, Integer.class);
		String result = restOperation.execute(client);
		assertEquals("55",result);
		
		new Verifications() {
			{
				client.execute(request, Integer.class);
				times=1;
				
				parser.parse(responseEntity);
				times=1;
			}
		};
	}
	
	public void testExchangeWithFloat() {
		new NonStrictExpectations() {
			{
				client.execute(request, String.class);
				result = responseEntity;
				
				parser.parse(responseEntity);
				result = 55;
			}
		};
		
		RestOperation<Integer, String> restOperation = new RestOperation<Integer, String>(request, parserIS, String.class);
		Integer result = restOperation.execute(client);
		assertEquals(Integer.valueOf(55),result);
		
		new Verifications() {
			{
				client.execute(request, String.class);
				times=1;
				
				parser.parse(responseEntity);
				times=1;
			}
		};
	}
	
}
