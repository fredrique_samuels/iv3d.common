/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.common.rest;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

public class HttpRestClientTest extends TestCase {
	private RestClient instance;
	@Mocked
	Request request;
	@Mocked 
	RestTemplate restTemplate;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new HttpRestClient(restTemplate);
	}
	
	public void testGetCalled() throws URISyntaxException {
		final HttpHeaders headers = new HttpHeaders();
		new NonStrictExpectations() {
			{
				request.getUrl();
				result = new URI("https://localhost:8888/welcom.jsp");
				
				request.getMethod();
				result = HttpMethod.GET;
				
				request.getHeaders();
				result = headers;
				
				request.getPayload();
				result = null;
			}
		};
		
		instance.execute(request, String.class);
		
		new Verifications() {
			{
				restTemplate.exchange((URI)any, HttpMethod.GET, (HttpEntity<?>)any, String.class);
				times=1;
			}
		};
		
	}
}
